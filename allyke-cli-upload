#!/usr/bin/env node

var program = require('commander');
var config = require('config');
var chalk = require('chalk');
var fs = require('fs');
var https = require('https');
var AllykeApi = require('./lib/api');
var async = require('async');
var path = require('path');
var _ = require('underscore');

var uploadFile = null;
var defaultInterval = 1; // in minutes
program.process = null;

program
    .arguments('<upload_file>')
    .option('-p, --process [process]', 'true/false: Process product similarities when this upload file is complete (default: false)')
    .option('-a, --action [process_action]', 'append/replace: Append or replace existing data in the account (default: append)')
    .option('-c, --category [category]', 'Category value to use for contents of this file, if not found in file column')
    .option('-i, --interval [interval]', 'Interval at which to check for status updates, in minutes (default: '+defaultInterval+')')
    .action(function (upload_file) {
        uploadFile = upload_file;
    })
    .on('--help', function(){
        console.log('  Examples:');
        console.log('');
        console.log('    $ ./allyke-cli upload -i 5 -c "Default Category" /home/ubuntu/catalog/today-data-1.csv');
        console.log('    or');
        console.log('    $ ./allyke-cli upload -p true /home/ubuntu/catalog/today-data-2.csv');
        console.log('');
    })
    .parse(process.argv);


if(!uploadFile) {
    console.error('No data file specified. Quitting.');
    process.exit(1);
} else {
    uploadFile = path.resolve(__dirname, uploadFile);
}

try {
    fs.accessSync(uploadFile, fs.R_OK);
} catch(e) {
    console.error("Process cannot read specified input file: "+uploadFile);
    process.exit(1);
}

if(!config.get('credentials.api_key') || !config.get('credentials.api_secret')) {
    console.error('Unable to proceed without configuration details for API Key and API Secret in config/local.json.');
    process.exit(1);
}

if(program.process===null || program.process.toUpperCase()==='FALSE') {
    program.process = false;
} else if (program.process.toUpperCase()==='TRUE') {
    program.process = true;
}
if(!program.process_action) {
    program.process_action = 'append';
}
if(!program.interval) {
    program.interval = defaultInterval;
}

//create the file entry in the Allyke API
console.log("Ready to upload file from "+uploadFile+" to your account.");

var api = new AllykeApi();
var datetimeStart = Date.now();
var inputFileId;

var addInputFile = function(cb) {
    api.createInputFile({}, function(err, data) {
        if(err) {
            console.error("Unable to create the input file record.");
            process.exit(1);
        }
        inputFileId = data.id;
        console.log(chalk.green("Input File record created (ID: "+inputFileId+")."));
        console.log(chalk.green("Posting data file to "+data.payload_url));
        api.postInputFile(data.payload_url, uploadFile, {action: program.process_action, process_immediately: program.process}, function(err, data) {
            if(err) {
                console.error(chalk.red("Unable to post the file. "+err.message));
                process.exit(1);
            }
            console.log(chalk.green("Input file payload added."));
            return cb(false, data);
        });
    });
};

var _waitForInputFileStatus = function(cb) {
    setTimeout(function() {
        api.getInputFileStatus(inputFileId, function(err, data) {
            if(err) {
                return cb(err);
            }
            if(data && data.file && (data.file.status==="complete" || data.file.status==='error')) {
                if(data.file.status==='error') {
                    console.log(chalk.red("Input file status is Error. Please review log in error response."));
                    return cb(new Error(data.file.log));
                } else {
                    console.log(chalk.green("Input file status complete. "+data.file.num_products_complete+" Products completed, "+data.file.num_products_failed+" failures."));
                    return cb();
                }
            } else {
                console.log(chalk.blue("Input file status not yet complete: "+(data.file?data.file.status:"")));
                return _waitForInputFileStatus(cb);
            }
        });
    }, program.interval * 60 * 1000, cb);
};

var waitForInputFileStatus = function(cb) {
    //run every INTERVAL and check for input file status updates
    console.log(chalk.green('Waiting for input file processing completion.'));
    return _waitForInputFileStatus(cb);
};

var getAccountSummary = function(cb) {
    api.getClientStatus(function(err, data) {
        if(err) {
            console.log(chalk.red("Unable to get client status."));
            return cb(err);
        }
        var str = "Products Status Report: ";
        _.each(_.allKeys(data.products), function(key) {
            str += key+": "+data.products[key].count+", ";
        });
        console.log(chalk.green(str));
        return cb();
    });
};

async.series([
    addInputFile,
    waitForInputFileStatus,
    getAccountSummary
], function(err, results) {
    var processingTime = (Date.now() - datetimeStart)/1000;
    if(err) {
        console.error(chalk.red("Error uploading file. "+err.message));
        console.log("Processing time: "+processingTime+" seconds");
        process.exit(1);
    } else {
        console.log("Complete.");
        console.log("Processing time: "+processingTime+" seconds");
        process.exit(0);
    }
});
