var config = require('config');
var RestClient = require('node-rest-client').Client;
var restler = require('restler');
var fs = require('fs');
var _ = require('underscore');

var client = new RestClient();

var INPUT_FILE_ENDPOINT = 'input/file';
//var INPUT_FILE_PAYLOAD_ENDPOINT = 'input/file/payload/'; //payload URL is given in response from INPUT_FILE request
var CLIENT_STATUS_ENDPOINT = 'account/status';
var OUTPUT_FILE_ENDPOINT = 'output/file';
var GENERATE_ENDPOINT = 'generate';


var AllykeAPI = function() {
    this.baseParams = {
        api_key: config.get('credentials.api_key'),
        api_secret: config.get('credentials.api_secret')
    };
};

/**
 * Get the full client account status object
 * @param cb
 */
AllykeAPI.prototype.getClientStatus = function(cb) {
    this._clientGetClientStatus(JSONResponseParser('Get Client Status', cb));
};

/**
 * Create an input file record
 * @param options
 * @param cb
 */
AllykeAPI.prototype.createInputFile = function(options, cb) {
    this._clientCreateInputFile(options, JSONResponseParser('Create Input File', cb));
};

/**
 * Post the content to an existing, newly-created input file record
 * @param payload_url
 * @param localfile
 * @param options
 * @param cb
 */
AllykeAPI.prototype.postInputFile = function(payload_url, localfile, options, cb) {
    this._clientPostInputFile(payload_url, localfile, options, JSONResponseParser('Input File Payload', cb));
};

/**
 * Get the status of an input file record as it's processed
 * @param id
 * @param cb
 */
AllykeAPI.prototype.getInputFileStatus = function(id, cb) {
    this._clientGetInputFileStatus(id, JSONResponseParser('Get Input File Status', cb));
};

/**
 * Create an output file record
 * @param options
 * @param cb
 */
AllykeAPI.prototype.createOutputFile = function(options, cb) {
    this._clientCreateOutputFile(options, JSONResponseParser('Create Output File', cb));
};

/**
 * Get the status of an output file creation record
 * @param id
 * @param cb
 */
AllykeAPI.prototype.getOutputFileStatus = function(id, cb) {
    this._clientGetOutputFileStatus(id, JSONResponseParser('Get Output File Status', cb));
};

/**
 * Triggers a reprocessing of all pending products
 * @param cb
 */
AllykeAPI.prototype.generate = function(cb) {
    this._clientGenerate(JSONResponseParser('Generate', cb));
};



/**
 * Basic wrapper to API client calls; returns a function which returns either an error
 * in the callback or the response body as a JSON object.
 * Simplifies the public functions in the AllykeAPI object to remove duplicate code.
 * 
 * @param functionName
 * @param cb
 * @returns {Function}
 * @constructor
 */
var JSONResponseParser = function(functionName, cb) {
    return function(err, data) {
        if (err) {
            return cb(err);
        }
        if(Buffer.isBuffer(data)){
            data = JSON.parse(data.toString('utf8'));
        }
        if(!data || data.status.toUpperCase()!=="SUCCESS") {
            return cb(new Error(functionName+' call returned a non-success response:'+data.message));
        }
        return cb(err, data);
    }
};

/**
 * Call the Allyke API endpoint to get the client's account status
 * @param cb
 * @private
 */
AllykeAPI.prototype._clientGetClientStatus = function(cb) {

    var args = {
        data: this.baseParams,
        headers: {"Content-Type": "application/json"}
    };
    var req = client.get(config.get('host')+CLIENT_STATUS_ENDPOINT, args, function(data) {
        return cb(false, data);
    });
    req.on('error', cb);
};

/**
 * Call the Allyke API endpoint to create an input file record
 * @param params
 * @param cb
 * @private
 */
AllykeAPI.prototype._clientCreateInputFile = function(params, cb) {

    _.defaults(params, this.baseParams);

    var args = {
        data: params,
        headers: {"Content-Type": "application/json"}
    };
    var req = client.post(config.get('host')+INPUT_FILE_ENDPOINT, args, function(data) {
        return cb(false, data);
    });
    req.on('error', cb);
};

/**
 * Call the Allyke API endpoint to post the content to the newly created input file record
 * @param payload_url
 * @param filepath
 * @param options
 * @param cb
 * @private
 */
AllykeAPI.prototype._clientPostInputFile = function(payload_url, filepath, options, cb) {
    fs.stat(filepath, function(err, stats) {
        var data = {};
        _.defaults(data, {
            "file": restler.file(filepath, null, stats.size, 'utf8', 'text/plain')
        }, options);
        
        restler.post(payload_url, {
            multipart: true,
            data: data
        }).on('complete', function(data) {
            return cb(false, data);
        });
    });
};

/**
 * Call the Allyke API endpoint to get the status of an input file
 * @param id
 * @param cb
 * @private
 */
AllykeAPI.prototype._clientGetInputFileStatus = function(id, cb) {
    var params = {
        id: id
    };
    _.defaults(params, this.baseParams);

    var args = {
        parameters: params,
        headers: {"Content-Type": "application/json"}
    };
    var req = client.get(config.get('host')+INPUT_FILE_ENDPOINT, args, function(data) {
        return cb(false, data);
    });
    req.on('error', cb);
};

/**
 * Call the Allyke API endpoint to request a new output file
 * @param options
 * @param cb
 * @private
 */
AllykeAPI.prototype._clientCreateOutputFile = function(options, cb) {
    var params = {};
    _.defaults(params, options, this.baseParams);

    var args = {
        data: params,
        headers: {"Content-Type": "application/json"}
    };
    var req = client.post(config.get('host')+OUTPUT_FILE_ENDPOINT, args, function(data) {
        return cb(false, data);
    });
    req.on('error', cb);
};

/**
 * Call the Allyke API endpoint to get the status of an output file
 * @param id
 * @param cb
 * @private
 */
AllykeAPI.prototype._clientGetOutputFileStatus = function(id, cb) {
    var params = {
        id: id
    };
    _.defaults(params, this.baseParams);

    var args = {
        parameters: params,
        headers: {"Content-Type": "application/json"}
    };
    var req = client.get(config.get('host')+OUTPUT_FILE_ENDPOINT, args, function(data) {
        return cb(false, data);
    });
    req.on('error', cb);
};

/**
 * Call the Allyke API endpoint to generate
 * @param cb
 * @private
 */
AllykeAPI.prototype._clientGenerate = function(cb) {
    var args = {
        data: this.baseParams,
        headers: {"Content-Type": "application/json"}
    };
    var req = client.post(config.get('host')+GENERATE_ENDPOINT, args, function(data) {
        return cb(false, data);
    });
    req.on('error', cb);
};

module.exports = AllykeAPI;