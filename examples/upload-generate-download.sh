#!/bin/bash
# This example script uploads the CSV file found at the passed parameter,
# calls the Generate endpoint, then downloads the resulting similarity data
#
# Please note that this is only an example and in a production environment,
# you probably should compress the data file in a .gz format before uploading.
# If your catalog is split into multiple files, please see the documentation.
#
# This process can also take a long time to execute depending on the number of
# products in your catalog.

UPLOAD=$1

echo 'Upload ' UPLOAD

#./allyke-cli upload $1 && ./allyke-cli generate && ./allyke-cli download

echo Done